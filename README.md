# GitLab Program Management  

Instruction Project  
Zero to Hero Roadshow  

In this training session, participants will learn how to use GitLab to quickly manage agile planning across multiple projects, teams, and personas.

Please review the [issues](../../issues) for step-by-step instructions.
